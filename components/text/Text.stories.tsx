import React from "react";
import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";

export default {
  title: "Text",
};

export const Typography = () => (
  <ThemeProvider theme={defaultTheme}>
    <h1>Heading 1</h1>
    <h2>Heading 2</h2>
    <h3>Heading 3</h3>
    <h4>Heading 4</h4>
    <h5>Heading 5</h5>
    <h6>Heading 6</h6>
    <p>Regular</p>
  </ThemeProvider>
);

export const ArticleExample = () => (
  <ThemeProvider theme={defaultTheme}>
    <h1>Developing apps like a pro</h1>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at
      consequatur deserunt dolor est incidunt inventore laborum, magnam omnis
      praesentium quae ratione rem repellendus rerum veniam vitae voluptatem.
      Consequuntur, sunt.
    </p>
    <h5>Don't ever do it!</h5>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at
      consequatur deserunt dolor est incidunt inventore laborum, magnam omnis
      praesentium quae ratione rem repellendus rerum veniam vitae voluptatem.
      Consequuntur, sunt.
    </p>
    <h2>But here's what</h2>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aperiam
      delectus distinctio, ducimus ipsum magni numquam obcaecati placeat quo
      recusandae, rem suscipit vero. Ab distinctio doloribus eveniet facilis
      necessitatibus qui.
    </p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at
      consequatur deserunt dolor est incidunt inventore laborum, magnam omnis
      praesentium quae ratione rem repellendus rerum veniam vitae voluptatem.
      Consequuntur, sunt.
    </p>
  </ThemeProvider>
);
