import { withStyles } from "@material-ui/styles";
import React, { ReactElement, ReactNode } from "react";

import { IHasClassName } from "../../interfaces";
import clsx from "clsx";

interface IHasStyle {
  style: object;
}

interface IHasAnimation {
  onAnimationEnd: (e: AnimationEvent) => void;
}

interface IRenderProps extends IHasAnimation, IHasClassName {
  keepMounted: boolean;
  mounted: boolean;
  isOpen: boolean;
}

type RenderFunc = (props: IRenderProps) => ReactNode;

export interface IAnimatedMountProps
  extends IHasClassName,
    Partial<IHasAnimation> {
  /**
   * Do not remove component from tree on hide animation end
   */
  keepMounted?: boolean;

  /**
   * Makes children width and height 0
   */
  shrink?: boolean;
  classes?: Record<string, string>;
  /**
   * Should contain properties:
   * root - idle element state
   * open - contains animation for opening
   * close = contains animation for closing
   */
  animationClasses: Record<string, string>;
  delay?: number;
  isOpen?: boolean;
  children?: ReactNode | RenderFunc;
}

interface IAnimatedMountState {
  didMount: boolean;
  mounted?: boolean;
}

class AnimatedMount extends React.Component<
  IAnimatedMountProps,
  IAnimatedMountState
> {
  static displayName = "AnimatedMount";

  constructor(props: IAnimatedMountProps) {
    super(props);

    this.state = {
      didMount: false,
      mounted: !!props.isOpen,
    };
  }

  componentDidMount = () => {
    this.setState({ didMount: true });
  };

  componentDidUpdate(prevProps: Readonly<IAnimatedMountProps>) {
    if (!prevProps.isOpen && this.props.isOpen) {
      this.setState({ mounted: true });
    }
  }

  handleAnimationEnd = (e: AnimationEvent) => {
    e.stopPropagation();
    if (!this.props.isOpen) {
      this.setState({ mounted: false });
    }
    if (this.props.onAnimationEnd) {
      this.props.onAnimationEnd(e);
    }
  };

  render() {
    const { mounted, didMount } = this.state;
    const {
      children,
      animationClasses,
      classes,
      isOpen,
      className,
      keepMounted,
      shrink,
    } = this.props;

    const animationStyles = clsx(
      animationClasses.root,
      isOpen ? animationClasses.open : animationClasses.close,
      className
    );
    const isVisible = (mounted && didMount) || keepMounted;

    if (typeof children === "function") {
      const func = children as RenderFunc;
      return (
        <>
          {func({
            keepMounted: !!keepMounted,
            mounted: !!mounted,
            isOpen: !!isOpen,
            onAnimationEnd: this.handleAnimationEnd,
            className: clsx(
              shrink && !isVisible && classes?.shrinked,
              animationStyles
            ),
          })}
        </>
      );
    }

    return (
      <>
        {isVisible &&
          React.Children.map(
            children as any,
            (
              child: ReactElement<IHasClassName & IHasAnimation & IHasStyle>
            ) => {
              return React.cloneElement(child, {
                ...child.props,
                className: clsx(
                  child.props.className,
                  shrink && !isVisible && classes?.shrinked,
                  animationStyles
                ),
                onAnimationEnd: this.handleAnimationEnd,
                style: { ...child.props.style, animationFillMode: "both" },
              });
            }
          )}
      </>
    );
  }
}

export default withStyles({
  shrinked: {
    maxWidth: 0,
    maxHeight: 0,
    padding: 0,
    margin: 0,
  },
})(AnimatedMount);
