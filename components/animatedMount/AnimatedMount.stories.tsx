import React from "react";
import { withKnobs, boolean, optionsKnob } from "@storybook/addon-knobs";
import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";
import { Box } from "../box";
import { Column } from "../flexBox";
import Grow from "./Grow";
import Slide from "./Slide";
import Fade from "./Fade";

export default {
  title: "Animated Mount",
  decorators: [withKnobs],
};

const origins = {
  left: "left",
  right: "right",
  top: "top",
  bottom: "bottom",
  center: "center",
  inherit: "inherit",
  initial: "initial",
  unset: "unset",
};

export const scaleInAndOut = () => (
  <ThemeProvider theme={defaultTheme}>
    <Grow
      keepMounted
      isOpen={boolean("Is open", true)}
      origin={optionsKnob("Transform origin", origins, origins["left"] as any, {
        display: "radio",
      })}
    >
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero fugit
        voluptatem minima, eaque facere non delectus velit harum ullam corrupti,
        odit culpa omnis mollitia eius, optio at incidunt alias suscipit!
      </p>
    </Grow>
    <p>
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Non maxime at
      libero quibusdam ad quod dolores ab nesciunt provident optio id, fuga
      excepturi impedit, minus eaque! Numquam cum quia alias.
    </p>
  </ThemeProvider>
);

const directions = {
  left: "left",
  right: "right",
  top: "top",
  bottom: "bottom",
};

export const slideInAndOut = () => (
  <ThemeProvider theme={defaultTheme}>
    <Slide
      keepMounted
      isOpen={boolean("Is open", true)}
      direction={optionsKnob("Direction", directions, directions.top as any, {
        display: "radio",
      })}
    >
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero fugit
        voluptatem minima, eaque facere non delectus velit harum ullam corrupti,
        odit culpa omnis mollitia eius, optio at incidunt alias suscipit!
      </p>
    </Slide>
    <p>
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Non maxime at
      libero quibusdam ad quod dolores ab nesciunt provident optio id, fuga
      excepturi impedit, minus eaque! Numquam cum quia alias.
    </p>
  </ThemeProvider>
);

export const fadeInAndOut = () => (
  <ThemeProvider theme={defaultTheme}>
    <Fade keepMounted isOpen={boolean("Is open", true)}>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero fugit
        voluptatem minima, eaque facere non delectus velit harum ullam corrupti,
        odit culpa omnis mollitia eius, optio at incidunt alias suscipit!
      </p>
    </Fade>
    <p>
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Non maxime at
      libero quibusdam ad quod dolores ab nesciunt provident optio id, fuga
      excepturi impedit, minus eaque! Numquam cum quia alias.
    </p>
  </ThemeProvider>
);
