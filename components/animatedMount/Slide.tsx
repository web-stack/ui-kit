import React from "react";
import { ITheme } from "../../styles/themes/ITheme";
import AnimatedMount, { IAnimatedMountProps } from "./AnimatedMount";
import { makeStyles } from "@material-ui/styles";
import "./Animations.css";

type Direction = "left" | "right" | "top" | "bottom";

interface IStyleProps {
  delay?: string;
  direction: Direction;
}

const useStyles = makeStyles<ITheme, IStyleProps>((theme: ITheme) => ({
  root: {},
  open: {
    animationName: (props) => `Slide-open-${props.direction}`,
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayIn,
    animationDuration: () => theme.animations.easeInOut.durationIn,
    animationTimingFunction: () => theme.animations.easeInOut.easingIn,
  },
  close: {
    animationName: (props) => `Slide-close-${props.direction}`,
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayOut,
    animationDuration: () => theme.animations.easeInOut.durationOut,
    animationTimingFunction: () => theme.animations.easeInOut.easingOut,
  },

  "@global": {
    "@keyframes Slide-open-left": {
      "0%": {
        transform: "translateX(-100vw)",
      },
      "100%": {
        transform: "translateX(0)",
      },
    },
    "@keyframes Slide-close-left": {
      "0%": {
        transform: "translateX(0)",
      },
      "100%": {
        transform: "translateX(-100vw)",
      },
    },

    "@keyframes Slide-open-top": {
      "0%": {
        transform: "translateY(-100vw)",
      },
      "100%": {
        transform: "translateY(0)",
      },
    },
    "@keyframes Slide-close-top": {
      "0%": {
        transform: "translateY(0)",
      },
      "100%": {
        transform: "translateY(-100vw)",
      },
    },

    "@keyframes Slide-open-right": {
      "0%": {
        transform: "translateX(100vw)",
      },
      "100%": {
        transform: "translateX(0)",
      },
    },
    "@keyframes Slide-close-right": {
      "0%": {
        transform: "translateX(0)",
      },
      "100%": {
        transform: "translateX(100vw)",
      },
    },

    "@keyframes Slide-open-bottom": {
      "0%": {
        transform: "translateY(100vw)",
      },
      "100%": {
        transform: "translatesY(0)",
      },
    },
    "@keyframes Slide-close-bottom": {
      "0%": {
        transform: "translateY(0)",
      },
      "100%": {
        transform: "translateY(100vw)",
      },
    },
  },
}));

export interface ISlideProps extends Partial<IAnimatedMountProps> {
  isOpen: boolean;
  direction: Direction;
  animationClasses?: Record<string, string>;
}

function Slide({ children, direction, delay, ...props }: ISlideProps) {
  const classes = useStyles({ direction, delay: `${delay}ms` });

  return (
    <AnimatedMount animationClasses={classes} {...props}>
      {children}
    </AnimatedMount>
  );
}

Slide.displayName = "Slide";

export default Slide;
