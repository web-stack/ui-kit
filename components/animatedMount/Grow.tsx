import { ITheme } from "../../styles/themes/ITheme";
import AnimatedMount, { IAnimatedMountProps } from "./AnimatedMount";

import React from "react";
import { makeStyles } from "@material-ui/styles";

type TransitionOrigin =
  | "left"
  | "right"
  | "center"
  | "top"
  | "bottom"
  | "inherit"
  | "initial"
  | "unset";

interface IStyleProps {
  delay?: string;
  origin?: TransitionOrigin;
}

const useStyles = makeStyles<ITheme, IStyleProps>((theme: ITheme) => ({
  root: {
    transformOrigin: (props) => props.origin,
  },
  open: {
    animationName: "$open",
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayIn,
    animationDuration: theme.animations.easeInOut.durationIn,
    animationTimingFunction: theme.animations.easeInOut.easingIn,
  },
  close: {
    animationName: "$close",
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayOut,
    animationDuration: theme.animations.easeInOut.durationOut,
    animationTimingFunction: theme.animations.easeInOut.easingOut,
  },
  "@keyframes open": {
    from: {
      opacity: 0,
      transform: "scale(0, 0)",
    },
    to: {
      opacity: 1,
      transform: "scale(1, 1)",
    },
  },
  "@keyframes close": {
    from: {
      opacity: 1,
      transform: "scale(1, 1)",
    },
    to: {
      opacity: 0,
      transform: "scale(0, 0)",
    },
  },
}));

export interface IGrowProps extends Partial<IAnimatedMountProps> {
  isOpen: boolean;
  origin?: TransitionOrigin;
  animationClasses?: Record<string, string>;
}

function Grow({ children, origin, delay, ...props }: IGrowProps) {
  const classes = useStyles({ origin, delay: `${delay}ms` });

  return (
    <AnimatedMount animationClasses={classes} {...props}>
      {children}
    </AnimatedMount>
  );
}

Grow.displayName = "Grow";

export default Grow;
