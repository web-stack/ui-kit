import "./Animations.css";
import { ITheme } from "../../styles/themes/ITheme";

import AnimatedMount, { IAnimatedMountProps } from "./AnimatedMount";

import React from "react";
import { makeStyles } from "@material-ui/styles";

interface IStyleProps {
  delay?: string;
}

const useStyles = makeStyles<ITheme, IStyleProps>((theme: ITheme) => ({
  root: {},
  open: {
    animationName: "$fade-in",
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayIn,
    animationDuration: theme.animations.easeInOut.durationIn,
    animationTimingFunction: theme.animations.easeInOut.easingIn,
    animationFillMode: "both",
  },
  close: {
    animationName: "$fade-out",
    animationDelay: (props) =>
      props.delay || theme.animations.easeInOut.delayOut,
    animationDuration: theme.animations.easeInOut.durationOut,
    animationTimingFunction: theme.animations.easeInOut.easingOut,
    animationFillMode: "both",
  },
  "@keyframes fade-in": {
    "0%": {
      opacity: 0,
    },
    "100%": {
      opacity: 1,
    },
  },
  "@keyframes fade-out": {
    "0%": {
      opacity: 1,
    },
    "100%": {
      opacity: 0,
    },
  },
}));

export interface IFadeProps extends Partial<IAnimatedMountProps> {
  isOpen: boolean;
  animationClasses?: Record<string, string>;
}

function Fade({ children, delay, ...props }: IFadeProps) {
  const classes = useStyles({ delay: `${delay}ms` });

  return (
    <AnimatedMount animationClasses={classes} {...props}>
      {children}
    </AnimatedMount>
  );
}

Fade.displayName = "Fade";

export default Fade;
