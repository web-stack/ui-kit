import { makeStyles } from "@material-ui/styles";
import { ITheme } from "../../styles/themes/ITheme";

const useStyles = makeStyles<ITheme>((theme) => ({
  root: {
    ...theme.typography.variants.regular,

    outline: "none",
    border: "none",
    width: "100%",
    cursor: "pointer",
    padding: `0.8em 1em`,
    backgroundColor: theme.palette.light.primary.main,
    color: theme.palette.light.text.light,
    borderRadius: theme.shapes.borderRadius,
    textAlign: "center",
    transition: "background-color 0.3s ease-out",
    margin: 0,

    "&:disabled": {
      backgroundColor: theme.palette.light.primary.light,
    },
  },
  active: {
    animationName: "$button-click",
    animationDuration: "0.15s",
    animationDirection: "alternate",
    animationTimingFunction: "ease-out",
    animationIterationCount: 2,
  },
  "@keyframes button-click": {
    "0%": {
      transform: "translateY(0)",
      backgroundColor: theme.palette.light.primary.main,
    },
    "100%": {
      transform: "translateY(8px)",
      backgroundColor: theme.palette.light.primary.dark,
    },
  },
}));

export default useStyles;
