import React, { ReactNode, useEffect, useState } from "react";
import clsx from "clsx";
import useStyles from "./Button.styles";

export interface IButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  children?: ReactNode;
}

function Button({ children, ...other }: IButtonProps) {
  const styles = useStyles();
  const ref = React.useRef<HTMLButtonElement>();
  const [touchState, setTouchState] = useState("idle");

  const handleClick = () => {
    setTouchState("clicked");
  };

  useEffect(() => {
    function animationEndHandler() {
      setTouchState("idle");
    }
    ref.current?.addEventListener("animationend", animationEndHandler);

    return () =>
      ref.current?.removeEventListener("animationend", animationEndHandler);
  }, []);

  return (
    <button
      {...other}
      ref={ref as any}
      onClick={handleClick}
      className={clsx(styles.root, touchState === "clicked" && styles.active)}
    >
      {children}
    </button>
  );
}

export default Button;
