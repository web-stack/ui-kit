import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";
import Button from "./Button";
import React from "react";

const { addons } = require("@ismithi/ui-builder/.storybook");

export default {
  title: "Button",
  decorators: [addons.knobs.withKnobs],
};

export const DefaultButton = () => (
  <ThemeProvider theme={defaultTheme}>
    <Button
      disabled={addons.knobs.boolean("Disabled", false)}
      children={addons.knobs.text("Text", "Press me!")}
    />
  </ThemeProvider>
);
