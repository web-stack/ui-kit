import * as React from "react";
import { Box } from "../box";
import FlexBox, { IFlexBoxProps } from "./FlexBox";
import clsx from "clsx";
import useFlexBoxStyles from "./FlexBox.styles";

export interface IColumnProps extends IFlexBoxProps {
  spacing?: number;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

function Column({
  children,
  spacing,
  direction = "column",
  ...props
}: IColumnProps) {
  const { root } = useFlexBoxStyles({ ...props, direction });
  const childCount = React.Children.count(children);

  return (
    <FlexBox {...props} className={clsx(root, props && props.className)}>
      {React.Children.map(children, (child: JSX.Element, i) => {
        if (!spacing) return child;

        if (child.type.name === "Box") {
          if (i === 0) {
            return React.cloneElement(child, { ...child.props, pb: spacing });
          }
          if (i === childCount - 1) {
            return React.cloneElement(child, { ...child.props, pt: spacing });
          }
        }

        return (
          <Box pt={i !== 0 && spacing} pb={i !== childCount - 1 && spacing}>
            {child}
          </Box>
        );
      })}
    </FlexBox>
  );
}

Column.displayName = "Column";

export default Column;
