import { number, text, withKnobs } from "@storybook/addon-knobs";
import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";
import { Box } from "../box";
import { Layout } from "../layout";
import FlexBox from "./FlexBox";
import * as React from "react";
import Column from "./Column";
import Row from "./Row";

export default {
  title: "Flex Box",
  decorators: [withKnobs],
};

export const example = () => (
  <ThemeProvider theme={defaultTheme}>
    <FlexBox
      alignContent={text("Align content", "flex-start")}
      alignItems={text("Align items", "flex-start")}
      justify={text("Justify", "flex-start")}
    >
      <Box p={2}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
      <Box p={2}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
      <Box p={2}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
    </FlexBox>
  </ThemeProvider>
);

export const withLayoutExample = () => (
  <ThemeProvider theme={defaultTheme}>
    <FlexBox
      alignContent={text("Align content", "flex-start")}
      alignItems={text("Align items", "flex-start")}
      justify={text("Justify", "flex-start")}
    >
      <Layout xs={10} sm={6}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad ea neque
          officia unde vel, voluptas voluptatem? Ad et repellat soluta. Ab
          architecto atque autem dolorem earum in laboriosam nihil, reiciendis!
        </p>
      </Layout>
      <Box p={2}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias
          aliquam aliquid assumenda aut corporis doloremque ducimus facilis
          ipsam nostrum optio, provident quibusdam quis quo sit unde vel, velit
          voluptatum.
        </p>
      </Box>
    </FlexBox>
  </ThemeProvider>
);

export const column = () => (
  <ThemeProvider theme={defaultTheme}>
    <Column spacing={number("Spacing", 2)}>
      <Box>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
      <Box>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
      <Box>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
          assumenda deleniti dolore ex laborum odit repellat voluptatum. Aliquid
          asperiores aspernatur corporis cumque eum, facere fuga hic modi
          molestias quo voluptatum.
        </p>
      </Box>
    </Column>
  </ThemeProvider>
);

export const simpleLayoutExample = () => (
  <ThemeProvider theme={defaultTheme}>
    <FlexBox>
      <Layout xs={12} md={8}>
        <Box p={2}>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam
            assumenda commodi consectetur cum, eum facilis id ipsa ipsum, magnam
            maiores nobis quam quia, ullam vero voluptate. Dolore explicabo
            nulla qui!
          </p>
          <p>some caption</p>
        </Box>
      </Layout>
      <Layout xs={12} md={4}>
        <Box p={2}>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam
            assumenda commodi consectetur cum, eum facilis id ipsa ipsum, magnam
            maiores nobis quam quia, ullam vero voluptate. Dolore explicabo
            nulla qui!
          </p>
        </Box>
      </Layout>
    </FlexBox>
  </ThemeProvider>
);

export const row = () => (
  <ThemeProvider theme={defaultTheme}>
    <Row spacing={2} wrap={false} grow>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem
        numquam aliquam dolor corporis nam necessitatibus voluptatibus ab
        cupiditate vel quibusdam. Cum eligendi, excepturi magni quod ipsum
        inventore corrupti commodi dicta.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem
        numquam aliquam dolor corporis nam necessitatibus voluptatibus ab
        cupiditate vel quibusdam. Cum eligendi, excepturi magni quod ipsum
        inventore corrupti commodi dicta.
      </p>
    </Row>
  </ThemeProvider>
);
