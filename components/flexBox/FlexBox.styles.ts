import { makeStyles } from "@material-ui/styles";
import { IFlexBox } from "../../interfaces";
import { ITheme } from "../../styles/themes/ITheme";

const useFlexBoxStyles = makeStyles<ITheme, IFlexBox>({
  root: {
    display: () => "flex",
    alignItems: ({ alignItems }) => alignItems,
    alignContent: ({ alignContent }) => alignContent,
    justifyContent: ({ justify }) => justify,
    flexWrap: ({ wrap = true }) => (wrap ? "wrap" : "nowrap"),
    flexDirection: ({ direction }) => direction,
    flexGrow: ({ grow }) => (grow ? 1 : 0),
  },
});

export default useFlexBoxStyles;
