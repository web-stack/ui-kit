import * as React from "react";
import { IFlexBox, IHasClassName } from "../../interfaces";
import useFlexBoxStyles from "./FlexBox.styles";
import clsx from "clsx";

export interface IFlexBoxProps extends IFlexBox, IHasClassName {
  children: JSX.Element | JSX.Element[];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

function FlexBox({
  children,
  className,
  alignContent,
  alignItems,
  justify,
  direction,
  wrap,
  grow,
  ...props
}: IFlexBoxProps) {
  const styles = useFlexBoxStyles({
    alignContent,
    alignItems,
    justify,
    direction,
    wrap,
    grow,
  });

  return (
    <section className={clsx(className, styles.root)} {...props}>
      {children}
    </section>
  );
}

FlexBox.displayName = "FlexBox";

export default FlexBox;
