import * as React from "react";
import { Box } from "../box";
import FlexBox, { IFlexBoxProps } from "./FlexBox";
import clsx from "clsx";

export interface IRowProps extends IFlexBoxProps {
  spacing?: number;
}

function Row({ children, spacing, ...props }: IRowProps) {
  const childCount = React.Children.count(children);
  const defaultProps: Partial<IFlexBoxProps> = {
    direction: "row",
    ...props,
  };

  return (
    <FlexBox {...defaultProps} className={clsx(props && props.className)}>
      {React.Children.map(children, (child: JSX.Element, i) => {
        if (!spacing) return child;

        if (child.type.name === "Box") {
          if (i === 0) {
            return React.cloneElement(child, { ...child.props, pl: spacing });
          }
          if (i === childCount - 1) {
            return React.cloneElement(child, { ...child.props, pr: spacing });
          }
        }

        return (
          <Box pl={i !== 0 && spacing} pr={i !== childCount - 1 && spacing}>
            {child}
          </Box>
        );
      })}
    </FlexBox>
  );
}

Row.displayName = "Row";

export default Row;
