import React from "react";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import FlexBox from "./FlexBox";
import Column from "./Column";

let container: HTMLDivElement = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

describe("<FlexBox/> test", () => {
  it("should render with no errors", async () => {
    act(() => {
      render(
        <FlexBox>
          <p>text</p>
        </FlexBox>,
        container
      );
    });

    expect(container.textContent).toEqual("text");
  });
});

describe("<Column/> test", () => {
  it("should render Column container with no errors", async () => {
    act(() => {
      render(
        <Column>
          <p>text</p>
        </Column>,
        container
      );

      expect(container.textContent).toEqual("text");
    });
  });
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});
