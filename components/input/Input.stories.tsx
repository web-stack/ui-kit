import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import defaultTheme from "../../styles/themes/default";
import Input from "./Input";

const { addons } = require("@ismithi/ui-builder/.storybook");

export default {
  title: "Input",
  decorators: [addons.knobs.withKnobs],
};

export const DefaultInput = () => (
  <ThemeProvider theme={defaultTheme}>
    <Input
      name="username"
      hasError={addons.knobs.boolean("Error", false)}
      disabled={addons.knobs.boolean("Disabled", false)}
      label={addons.knobs.text("Label", "Username")}
      value={addons.knobs.text("Value", "")}
    />
  </ThemeProvider>
);

export const InputsStack = () => (
  <ThemeProvider theme={defaultTheme}>
    <Input
      name="username"
      hasError={addons.knobs.boolean("Error", false)}
      disabled={addons.knobs.boolean("Disabled", false)}
      label={addons.knobs.text("Label", "Username")}
      value={addons.knobs.text("Value", "")}
    />
    <Input
      name="username"
      hasError={addons.knobs.boolean("Error", false)}
      disabled={addons.knobs.boolean("Disabled", false)}
      label={addons.knobs.text("Label", "Username")}
      value={addons.knobs.text("Value", "")}
    />
    <Input
      name="username"
      hasError={addons.knobs.boolean("Error", false)}
      disabled={addons.knobs.boolean("Disabled", false)}
      label={addons.knobs.text("Label", "Username")}
      value={addons.knobs.text("Value", "")}
    />
  </ThemeProvider>
);
