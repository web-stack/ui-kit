import { makeStyles } from "@material-ui/styles";
import { ITheme } from "../../styles/themes/ITheme";
import { IInputProps } from "./Input";

const useStyles = makeStyles<ITheme, IInputProps>((theme) => ({
  root: {
    position: "relative",

    "& > input:not(:placeholder-shown) + $placeholder": {
      transform: "translateY(-1em) scale(0.8)",
    },

    "& > input:focus + $placeholder": {
      transform: "translateY(-1em) scale(0.8)",
    },
  },
  input: (props) => ({
    boxSizing: "border-box",
    fontSize: "18px",
    width: "100%",
    border: "none",
    outline: "none",
    color: theme.palette.light.text.main,
    zIndex: 1,
    borderBottom: `2px solid ${
      props.hasError
        ? theme.palette.light.error.main
        : theme.palette.light.primary.main
    }`,
    transition: "border 0.3s ease-out, background-color 0.3s ease-out",
    padding: "1em 0 0",

    "&:disabled": {
      backgroundColor: `rgba(91, 91, 91, 0.2)`,
    },

    "&:focus": {
      borderBottom: `2px solid ${
        props.hasError
          ? theme.palette.light.error.dark
          : theme.palette.light.primary.dark
      }`,
    },
  }),
  placeholder: (props) => ({
    ...theme.typography.variants.caption,

    position: "absolute",
    bottom: "4px",
    left: 0,
    color: props.hasError
      ? theme.palette.light.error.main
      : theme.palette.light.gray.main,
    fontSize: "18px",
    userSelect: "none",

    transformOrigin: "left",
    transition: `transform 0.2s ease-out, color 0.2s ease-out`,
    cursor: "text",
    margin: 0,
  }),
}));

export default useStyles;
