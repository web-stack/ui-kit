import React, { useRef } from "react";
import useStyles from "./Input.styles";

export interface IInputProps {
  label: string;
  name: string;
  value?: string | number | string[];
  hasError?: boolean;
  type?: "text" | "password" | "date" | "datetime-local" | "time";
  disabled?: boolean;
  initial?: string | number | string[];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

function Input(props: IInputProps) {
  const inputStyles = useStyles(props);
  const input = useRef<HTMLInputElement>(null);
  const focusElement = () => {
    input.current?.focus();
  };

  return (
    <div className={inputStyles.root}>
      <input
        ref={input}
        type={props.type || "text"}
        name={props.name}
        placeholder=" "
        disabled={props.disabled}
        defaultValue={props.initial}
        value={props.value}
        onChange={props.onChange}
        className={inputStyles.input}
      />
      <label className={inputStyles.placeholder} onClick={focusElement}>
        {props.label}
      </label>
    </div>
  );
}

export default Input;
