import clsx from "clsx";
import React from "react";
import { IHasClassName, IHasChildren } from "../../interfaces";
import useBoxStyles from "./Box.styles";

export interface IBoxProps extends IHasClassName, IHasChildren {
  /**
   * Padding from **all** four sides
   */
  p?: number;

  /**
   * Padding applied to the **left** and **right** sides
   */
  px?: number;

  /**
   * Padding applied to the **top** and **bottom** sides
   */
  py?: number;

  /**
   * Padding applied only to the **top** side
   */
  pt?: number;

  /**
   * Padding applied only to the **bottom** side
   */
  pb?: number;

  /**
   * Padding applied only to the **right** side
   */
  pr?: number;

  /**
   * Padding applied only to the **left** side
   */
  pl?: number;

  /**
   * Defines if this element will have `flex-grow: 1` CSS property
   */
  grow?: boolean;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

function Box({
  children,
  className,
  p,
  px,
  py,
  pt,
  pb,
  pr,
  pl,
  grow,
  ...props
}: IBoxProps) {
  const styles = useBoxStyles({ p, pb, pt, px, py, pr, pl, grow });
  const classes = clsx(styles.root, className);

  return (
    <div {...props} className={classes}>
      {children}
    </div>
  );
}

Box.displayName = "Box";

export default Box;
