import * as React from "react";
import { number, withKnobs } from "@storybook/addon-knobs";
import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";
import { Box } from "./";

export default {
  title: "Box",
  decorators: [withKnobs],
};

export const paddingBox = () => (
  <ThemeProvider theme={defaultTheme}>
    <Box
      style={{ backgroundColor: "teal" }}
      p={number("Padding from all sides", 2)}
    >
      <p>Background displays padding</p>
    </Box>
  </ThemeProvider>
);

export const customPaddingBox = () => (
  <ThemeProvider theme={defaultTheme}>
    <Box
      style={{ backgroundColor: "teal" }}
      pt={number("Top padding", 2)}
      pb={number("Bottom padding", 2)}
      px={number("Horizontal padding", 2)}
      py={number("vertical padding", 2)}
    >
      <p>Background displays padding</p>
    </Box>
  </ThemeProvider>
);
