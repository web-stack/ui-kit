import { makeStyles } from "@material-ui/styles";
import { ITheme } from "../../styles/themes/ITheme";

interface IProps {
  p?: number;
  px?: number;
  py?: number;
  pt?: number;
  pb?: number;
  pr?: number;
  pl?: number;
  grow?: boolean;
}

const calculateSpacing = (spacing: number) => (value: number) => {
  return Math.floor(value) * spacing;
};

const useBoxStyles = makeStyles<ITheme, IProps>(((theme: ITheme) => {
  const calculate = calculateSpacing((theme && theme.shapes.spacing) || 0);

  return {
    root: {
      flexGrow: (props: IProps) => props.grow && 1,
      padding: (props: IProps) => props.p && calculate(props.p),
      paddingBottom: (props: IProps) =>
        (props.pb && calculate(props.pb)) || (props.py && calculate(props.py)),
      paddingLeft: (props: IProps) =>
        (props.pl && calculate(props.pl)) || (props.px && calculate(props.px)),
      paddingRight: (props: IProps) =>
        (props.pr && calculate(props.pr)) || (props.px && calculate(props.px)),
      paddingTop: (props: IProps) =>
        (props.pt && calculate(props.pt)) || (props.py && calculate(props.py)),
    },
  };
}) as any);

export default useBoxStyles;
