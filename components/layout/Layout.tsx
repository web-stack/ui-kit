import * as React from "react";
import { IHasBreakpoints, IHasChildren, IHasClassName } from "../../interfaces";
import { useBreakpointsStyles } from "../../styles/utilities";
import clsx from "clsx";
import { Box, IBoxProps } from "../box";

export interface ILayoutProps
  extends IHasChildren,
    IHasBreakpoints,
    IHasClassName {
  BoxProps?: IBoxProps;
}

function Layout({
  children,
  BoxProps,
  className,
  xs,
  sm,
  md,
  lg,
  xl,
  ...props
}: ILayoutProps) {
  const { breakpoints } = useBreakpointsStyles({ xs, sm, md, lg, xl });
  return (
    <Box
      {...props}
      {...BoxProps}
      className={clsx(breakpoints, className, BoxProps && BoxProps.className)}
    >
      {children}
    </Box>
  );
}

Layout.displayName = "Layout";

export default Layout;
