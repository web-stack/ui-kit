import * as React from "react";
import defaultTheme from "../../styles/themes/default";
import ThemeProvider from "../../styles/themes/ThemeProvider";
import Layout from "./Layout";
import { BreakpointValue } from "../../interfaces";

const { addons } = require("@ismithi/ui-builder/.storybook");

export default {
  title: "Layout",
  decorators: [addons.knobs.withKnobs],
};

export const controlledLayout = () => (
  <ThemeProvider theme={defaultTheme}>
    <Layout
      xs={addons.knobs.number("XS", 12) as BreakpointValue}
      sm={addons.knobs.number("SM", 10) as BreakpointValue}
      md={addons.knobs.number("MD", 8) as BreakpointValue}
      lg={addons.knobs.number("LG", 6) as BreakpointValue}
      xl={addons.knobs.number("XL", 6) as BreakpointValue}
      BoxProps={{ style: { backgroundColor: "teal" } }}
    >
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem
        consequatur fugiat nam nostrum perspiciatis qui sint sunt, veritatis.
        Excepturi ipsum magnam possimus quibusdam ullam? At iste laboriosam
        maxime officiis voluptate!
      </p>
    </Layout>
  </ThemeProvider>
);
