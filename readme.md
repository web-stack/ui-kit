# UI kit
A set of React components and utilities made with `@material-ui/styles`.

## Installation
`npm i @ismithi/ui-kit`

## Usage
Wrap your app with ThemeProvider:
```typescript jsx
import ThemeProvider from "@ismithi/ui-kit/styles/ThemeProvider"
import defaultTheme from "@ismithi/ui-kit/styles/themes/defaultTheme";

function App() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <MyComponents/>
    </ThemeProvider>
  )
}

export default App  
```

Then use components in your app:
```typescript jsx
import React from 'react'
import Button from "@ismithi/ui-kit/components/Button"

function App() {
  return (
    <Button>Press me</Button>
  )
}

export default App
```

## Developer commands
- `npm run storybook` - start a storybook dev server
- `npm run build-storybook` - create a static storybook bundle for deployment

