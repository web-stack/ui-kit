import { widthCalculator } from './useBreakpointsStyles'

describe('Breakpoints test', () => {
  it('should calculate correct width', () => {
    const calc = widthCalculator(12)

    expect(calc(6)).toEqual('50%')
    expect(calc(3)).toEqual('25%')
  })
})