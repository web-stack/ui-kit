import { makeStyles } from "@material-ui/styles";
import { IHasBreakpoints } from "../../interfaces";
import { ITheme } from "../themes/ITheme";

export const widthCalculator = (maxWidth: number) => (value: number) => {
  return value && `${(Math.floor(value) / maxWidth) * 100}%`;
};

const useStyles = makeStyles<ITheme, IHasBreakpoints>((theme) => {
  const getWidth = widthCalculator(12);

  return {
    [`@media screen and (min-width: ${theme.breakpoints.xs.minWidth}px)`]: {
      breakpoints: {
        maxWidth: ({ xs }) => getWidth(xs),
        minWidth: ({ xs }) => getWidth(xs),
      },
    },
    [`@media screen and (min-width: ${theme.breakpoints.sm.minWidth}px)`]: {
      breakpoints: {
        maxWidth: ({ sm }) => getWidth(sm),
        minWidth: ({ sm }) => getWidth(sm),
      },
    },
    [`@media screen and (min-width: ${theme.breakpoints.md.minWidth}px)`]: {
      breakpoints: {
        maxWidth: ({ md }) => getWidth(md),
        minWidth: ({ md }) => getWidth(md),
      },
    },
    [`@media screen and (min-width: ${theme.breakpoints.lg.minWidth}px)`]: {
      breakpoints: {
        maxWidth: ({ lg }) => getWidth(lg),
        minWidth: ({ lg }) => getWidth(lg),
      },
    },
    [`@media screen and (min-width: ${theme.breakpoints.xl.minWidth}px)`]: {
      breakpoints: {
        maxWidth: ({ xl }) => getWidth(xl),
        minWidth: ({ xl }) => getWidth(xl),
      },
    },
  };
});

export default useStyles;
