import { CSSProperties } from "@material-ui/styles";

interface IColor {
  main: string;
  light: string;
  dark: string;
}

interface IPalette {
  primary: IColor;
  secondary: IColor;
  gray: IColor;
  text: IColor;

  [key: string]: IColor;
}

interface ITypographyVariant extends CSSProperties {}

interface ITypography {
  fontFamilies: {
    primary: string;

    [key: string]: string;
  };
  variants: {
    h1: ITypographyVariant;
    h2: ITypographyVariant;
    h3: ITypographyVariant;
    h4: ITypographyVariant;
    h5: ITypographyVariant;
    h6: ITypographyVariant;
    regular: ITypographyVariant;
    caption: ITypographyVariant;

    [key: string]: ITypographyVariant;
  };
}

interface IShapes {
  borderRadius: string;
  spacing: number;
}

interface IAnimations {
  [key: string]: {
    easingIn?: string;
    easingOut?: string;
    durationIn?: string;
    durationOut?: string;
    delayIn?: string;
    delayOut?: string;
  };
}

export interface IBreakpointProps {
  minWidth: number;
}

export interface IBreakpoints {
  xs: IBreakpointProps;
  sm: IBreakpointProps;
  md: IBreakpointProps;
  lg: IBreakpointProps;
  xl: IBreakpointProps;
}

export interface ITheme {
  palette: {
    light: IPalette;
    dark?: IPalette;
  };
  typography: ITypography;
  shapes: IShapes;
  animations: IAnimations;
  breakpoints: IBreakpoints;
}
