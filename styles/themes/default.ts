import { CSSProperties } from "@material-ui/styles";
import { ITheme } from "./ITheme";

const commonTypographyProps: CSSProperties = {
  margin: "0.6em 0 0.3em 0",
  fontFamily: "-apple-system, Roboto, sans-serif",
};

const defaultTheme: ITheme = {
  palette: {
    light: {
      primary: {
        main: "#3795ec",
        light: "#B5D4F0",
        dark: "#1071cd",
      },
      secondary: {
        main: "#3795ec",
        light: "#B5D4F0",
        dark: "#1071cd",
      },
      gray: {
        main: "#758385",
        light: "#ABB1B1",
        dark: "#444D4F",
      },
      text: {
        main: "#1c1c1c",
        light: "#fff",
        dark: "#000",
      },
      error: {
        main: "#cf0e0e",
        light: "#ef2b2b",
        dark: "#980b0b",
      },
    },
  },
  typography: {
    fontFamilies: {
      primary: commonTypographyProps.fontFamily + "",
    },
    variants: {
      h1: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "3.2rem",
      },
      h2: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "2.6rem",
      },
      h3: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "2rem",
      },
      h4: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "1.5rem",
      },
      h5: {
        ...commonTypographyProps,
        fontWeight: "bold",
        fontSize: "1.1rem",
      },
      h6: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "0.8rem",
      },
      regular: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "1rem",
      },
      caption: {
        ...commonTypographyProps,
        fontWeight: "normal",
        fontSize: "0.8em",
        color: "#484848",
      },
    },
  },
  shapes: {
    borderRadius: "16px",
    spacing: 4,
  },
  breakpoints: {
    xs: { minWidth: 0 },
    sm: { minWidth: 600 },
    md: { minWidth: 960 },
    lg: { minWidth: 1280 },
    xl: { minWidth: 1920 },
  },
  animations: {
    swingInOut: {
      easingIn: "cubic-bezier(0.5, 0, 0.5, 1.6)",
      easingOut: "cubic-bezier(0.5, -0.6, 0.5, 0)",
      durationIn: "0.5s",
      durationOut: "0.4s",
    },
    easeInOut: {
      easingIn: "ease-out",
      easingOut: "ease-in",
      durationIn: "0.4s",
      durationOut: "0.3s",
    },
  },
};

export default defaultTheme;
