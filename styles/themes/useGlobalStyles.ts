import { makeStyles } from "@material-ui/styles";
import { ITheme } from "./ITheme";

const useGlobalStyles = makeStyles<ITheme>((theme) => ({
  "@global": {
    "*": {
      color: theme.palette.light.text.main,
      fontFamily: theme.typography.fontFamilies.primary,
    },
    h1: {
      ...theme.typography.variants.h1,
    },
    h2: {
      ...theme.typography.variants.h2,
    },
    h3: {
      ...theme.typography.variants.h3,
    },
    h4: {
      ...theme.typography.variants.h4,
    },
    h5: {
      ...theme.typography.variants.h5,
    },
    h6: {
      ...theme.typography.variants.h6,
    },
    p: {
      ...theme.typography.variants.regular,
    },
  },
}));

export default useGlobalStyles;
