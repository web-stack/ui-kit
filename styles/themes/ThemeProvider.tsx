import React, { ReactNode } from "react";
import { ThemeProvider as MUIThemeProvider } from "@material-ui/styles";
import defaultTheme from "./default";
import { ITheme } from "./ITheme";
import useGlobalStyles from "./useGlobalStyles";

interface IProps {
  theme: ITheme;
  children: ReactNode;
}

function ThemeProvider({ theme = defaultTheme, children }: IProps) {
  return (
    <MUIThemeProvider theme={theme}>
      <GlobalStyles>{children}</GlobalStyles>
    </MUIThemeProvider>
  );
}

interface IGlobalStylesProps {
  children: ReactNode;
}

function GlobalStyles(props: IGlobalStylesProps) {
  useGlobalStyles();

  return <>{props.children}</>;
}

export default ThemeProvider;
