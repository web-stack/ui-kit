module.exports = {
  // directory in which bundle is created
  output: "dist",

  // dev server port
  port: 8080,

  // index.html template location,
  // leave undefined to use pre defined template
  template: undefined,

  storybook: {
    componentsDir: "./",
  },
};
