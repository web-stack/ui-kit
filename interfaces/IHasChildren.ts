import React from "react"

export default interface IHasChildren {
  children?: React.ReactNode | string;
}