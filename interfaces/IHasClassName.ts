export default interface IHasClassName {
  className?: string;
}