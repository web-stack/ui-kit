export default interface IFlexBox {
  grow?: boolean;
  wrap?: boolean;
  direction?: "column" | "row" | "column-reverse" | "row-reverse";

  alignItems?:
    | "center"
    | "baseline"
    | "flex-start"
    | "flex-end"
    | "inherit"
    | "stretch"
    | string;

  alignContent?:
    | "center"
    | "end"
    | "start"
    | "flex-start"
    | "flex-end"
    | "normal"
    | "baseline"
    | "space-between"
    | "space-around"
    | "space-evenly"
    | "stretch"
    | string;

  justify?:
    | "center"
    | "flex-start"
    | "flex-end"
    | "space-between"
    | "space-around"
    | "space-evenly"
    | string;
}
